package models

type (
	// Transaction represents the collection structure in the DB
	Transaction struct {
		CommentID   string `bson:"comment_id"`
		Link        string `bson:"link"`
		Payed       bool   `bson:"payed"`
		Notified    bool   `bson:"notified"`
		Confirmed   bool   `bson:"confirmed"`
		Payment     int    `bson:"payment"`
		Post        string `bson:"post_id"`
		User        string `bson:"user_id"`
		StrangeUser string `bson:"buyer_id"`
		Page        string `bson:"page_id"`
	}

	// Profile represents the collection structure in the DB
	Profile struct {
		ID        string `bson:"user_id"`
		Name      string `bson:"name"`
		Email     string `bson:"email"`
		FirstName string `bson:"first_name"`
		LastName  string `bson:"last_name"`
		URL       string `bson:"url"`
		Gender    string `bson:"gender"`
	}

	// User represents the collection structure in the DB
	User struct {
		Profile        Profile `bson:"profile"`
		AccessToken    string  `bson:"access_token"`
		Pages          []Page  `bson:"pages"`
		DefaultPayment int     `bson:"default_payment"`
	}

	// Page represents the collection with IDs
	Page struct {
		ID          string `bson:"page_id"`
		AccessToken string `bson:"access_token"`
	}
)
