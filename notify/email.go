package notify

type SmtpTemplateData struct {
	From    string
	To      string
	Subject string
	Body    string
}

// Go template
const emailTemplate = `From: {{.From}}
To: {{.To}}
Subject: {{.Subject}}

{{.Body}}

Sincerely,

{{.From}}
`

// Authenticate with Gmail (analagous to logging in to your Gmail account in the browser)
