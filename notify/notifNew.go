package notify

import (
	"fmt"
	"io/ioutil"
	"masterlike-notifier/models"
	"net/http"
	"net/url"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// Notify represents the shared data between notifiers
type Notify struct {
	Transactions *mgo.Collection
	Users        *mgo.Collection
	Pages        *mgo.Collection
}

//NewTransactions represents the search for recent transactions to send to Facebook Notifier
func (n *Notify) NewTransactions() {

	for {
		var results []models.Transaction
		err := n.Transactions.Find(bson.M{"notified": false}).All(&results)
		if err != nil {
			panic(err)
		}

		// fmt.Println(results)

		for _, transaction := range results {

			accessToken := struct {
				Val string `bson:"access_token"`
			}{}

			// fmt.Println("Page String: ", transaction.Page)
			// NOTE Pegar access token para id de usuario fornecido
			err := n.Pages.Find(bson.M{"page_id": transaction.Page}).Select(bson.M{"access_token": 1}).One(&accessToken)
			if err != nil {
				panic(err)
				// continue
			}
			// fmt.Println("AccessToken: ", accessToken)

			// NOTE Notifica no facebook nova transacao
			err = n.notifyLinkFacebook(accessToken.Val, transaction.CommentID, transaction.Link)
			if err != nil {
				// panic(err)
				continue
			}

			// NOTE Update table with notified true
			err = n.Transactions.Update(
				bson.M{"comment_id": transaction.CommentID},
				bson.M{"$set": bson.M{"notified": true}},
			)
			if err != nil {
				// panic(err)
				continue
			}

		}
	}
}

func (n *Notify) notifyLinkFacebook(accessToken string, commentID string, link string) error {

	message := "Seja Patrocinador! Complete sua transação aqui: " + link

	var URL *url.URL
	URL, err := url.Parse("https://graph.facebook.com/v2.6")
	URL.Path += fmt.Sprintf("/%s/comments", commentID)
	parameters := url.Values{}
	parameters.Add("access_token", accessToken)
	parameters.Add("message", message)
	URL.RawQuery = parameters.Encode()

	// Create POST request for Profile service to create user and return profile
	fmt.Println(URL.String())
	req, _ := http.NewRequest("POST",
		URL.String(),
		nil,
	)
	req.Header.Set("Content-Type", "application/json")

	// Execute http POST call
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Parse response from body and print.
	_, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	// fmt.Println(string(body))

	return nil
}
