package notify

import (
	"bytes"
	"fmt"
	"html/template"
	"log"
	"masterlike-notifier/models"
	"net/smtp"

	"gopkg.in/mgo.v2/bson"
)

const (
	email       = "leonardogcsoares93@gmail.com"
	password    = "leo-42551625"
	emailServer = "smtp.gmail.com"
	emailPort   = "25"
)

var auth = smtp.PlainAuth(
	"",
	email,
	password,
	emailServer,
)

// FinalizedTransactions represents the search for payment transactions to send to Facebook Notifier
func (n *Notify) FinalizedTransactions() {

	for {
		var results []models.Transaction
		err := n.Transactions.Find(bson.M{"notified": true, "payed": true, "confirmed": false}).All(&results)
		if err != nil {
			// panic(err)
			return
		}
		// fmt.Println(results)
		for _, transaction := range results {

			var user models.User
			fmt.Println("User :", transaction.StrangeUser)
			// NOTE Colher valor pago, nome do usuario, email e mandar email
			err := n.Users.Find(bson.M{"profile.user_id": transaction.StrangeUser}).One(&user)
			fmt.Println("User:", user)
			if err != nil {
				// panic(err)
				continue
			}

			// TODO TEST mandar email para usuario
			n.notifyEmailConfirmation(user, transaction)

			// NOTE Update table with notified true
			err = n.Transactions.Update(
				bson.M{"comment_id": transaction.CommentID},
				bson.M{"$set": bson.M{"confirmed": true}},
			)
			if err != nil {
				// panic(err)
				continue
			}

		}
	}

}

func (n *Notify) notifyEmailConfirmation(user models.User, transaction models.Transaction) error {

	// NOTE New way of doing things
	var doc bytes.Buffer
	// Set the context for the email template.
	context := &SmtpTemplateData{"MasterLike",
		user.Profile.FirstName + " " + user.Profile.LastName + " <" + "chagas.0@gmail.com" + ">",
		"MasterLike - Confirmation of Payment",
		fmt.Sprintf("Hey %s,\n\nSeu patrocínio de R$%v para %s foi concluído com successo."+
			" Qualquer problema ou dúvida nos avisem, e resolveremos o mais rápido possível!",
			user.Profile.FirstName, transaction.Payment, transaction.Link,
		)}

	// Create a new template for our SMTP message.
	t := template.New("emailTemplate")
	if _, err := t.Parse(emailTemplate); err != nil {
		log.Print("error trying to parse mail template ", err)
	}

	// Apply the values we have initialized in our struct context to the template.
	if err := t.Execute(&doc, context); err != nil {
		log.Print("error trying to execute mail template ", err)
	}

	err := smtp.SendMail(
		emailServer+":"+emailPort,
		auth,
		"leonardogcsoares93@gmail.com",
		[]string{"chagas.0@gmail.com"},
		doc.Bytes())
	if err != nil {
		log.Print("ERROR: attempting to send a mail ", err)
	}

	return nil
}
