package main

import (
	"masterlike-notifier/notify"

	"gopkg.in/mgo.v2"
)

const (
	mongoDBHosts = "aws-us-east-1-portal.19.dblayer.com:10348"
	authDatabase = "masterlike"
	authUserName = "master"
	authPassword = "like123"
	testDatabase = "masterlike"
)

func main() {

	session, err := mgo.Dial("mongodb://master:like123@aws-us-east-1-portal.19.dblayer.com:10348/masterlike")
	if err != nil {
		panic(err)
	}

	err = session.Ping()
	if err != nil {
		panic(err)
	}
	defer session.Close()

	transactions := session.DB("masterlike").C("transactions")
	users := session.DB("masterlike").C("users")
	pages := session.DB("masterlike").C("pages")

	notif := notify.Notify{
		Transactions: transactions,
		Users:        users,
		Pages:        pages,
	}

	// Executa funcao notifyNew - separate thread
	// Recebe conexao do mongoDb
	go notif.FinalizedTransactions()
	notif.NewTransactions()

}
