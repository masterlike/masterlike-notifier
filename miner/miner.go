package miner

import "gopkg.in/mgo.v2"

// Miner represents the shared data between data mining
type Miner struct {
	Users       *mgo.Collection
	Likes       *mgo.Collection
	Movies      *mgo.Collection
	Music       *mgo.Collection
	Television  *mgo.Collection
	Subscribers *mgo.Collection
}
